//go:generate mockgen -source ${GOFILE} -destination mocks_test.go -package ${GOPACKAGE}_test
package service

import "context"

type repo interface {
	GetUrl(ctx context.Context,surl string) (string, error)
}

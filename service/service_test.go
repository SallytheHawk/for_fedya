package service_test

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	. "grpcProject/service"
)

func TestService_DoSmth(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		in       string
		prepare  func(mock *Mockrepo)
		expected func(t assert.TestingT, res string, err error)
	}{
		{
			name: "Success",
			in:   "lol",
			prepare: func(mock *Mockrepo) {
				mock.EXPECT().GetUrl(gomock.Any(), "lol").Return("kek", nil)
			},
			expected: func(t assert.TestingT, res string, err error) {
				assert.Equal(t, res, "kek")
				assert.NoError(t, err)
			},
		},
		{
			name: "Failed",
			in:   "kek",
			prepare: func(mock *Mockrepo) {
				mock.EXPECT().GetUrl(gomock.Any(), "kek").Return("", errors.New("pizdec"))
			},
			expected: func(t assert.TestingT, res string, err error) {
				assert.Error(t, err)
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			ctrl := gomock.NewController(t)

			repo := NewMockrepo(ctrl)
			tc.prepare(repo)

			obj := NewService(repo)
			ans, err := obj.DoSmth(context.Background(), tc.in)
			tc.expected(t, ans, err)
		})
	}
}

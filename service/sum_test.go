package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSum(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		a        int64
		b        int64
		expected func(t assert.TestingT, res int64)
	}{
		{
			name: "Success",
			a:    1,
			b:    2,
			expected: func(t assert.TestingT, res int64) {
				assert.Equal(t, res, int64(3))
			},
		},
	}

	for _, tc := range testCases {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			res := sum(tc.a, tc.b)
			tc.expected(t, res)
		})

	}
}

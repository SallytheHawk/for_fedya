package service

import (
	"context"
	"errors"

	"grpcProject/model"
)

type Service struct {
	repo repo
}

func NewService(repo repo) *Service {
	return &Service{repo: repo}
}

func (s *Service) DoSmth(ctx context.Context, url string) (string, error) {
	ans, err := s.repo.GetUrl(ctx, url)
	if errors.Is(err, model.ErrNotFound) {
		return "", nil
	}
	if err != nil {
		return "", err
	}
	return ans, nil
}

func sum(a, b int64) int64 {
	return a + b
}

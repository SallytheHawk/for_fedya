package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"

	"grpcProject/pb"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type server struct {
	pb.UnsafeSurlServer
}

func (s *server) AddUrl(ctx context.Context, in *pb.Url) (*pb.ShortUrl, error) {
	fmt.Println(in.Url)
	return &pb.ShortUrl{Url: in.Url + " jopa"}, nil
}

func (s *server) GetUrl(ctx context.Context, in *pb.ShortUrl) (*pb.Url, error) {
	fmt.Println(in.Url)
	return &pb.Url{Url: in.Url + " X2"}, nil
}

func runRest() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	err := pb.RegisterSurlHandlerFromEndpoint(ctx, mux, "localhost:12201", opts)
	if err != nil {
		panic(err)
	}
	log.Printf("server listening at 8081")
	if err := http.ListenAndServe(":8081", mux); err != nil {
		panic(err)
	}
}

func runGrpc() {
	lis, err := net.Listen("tcp", ":12201")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterSurlServer(s, &server{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		panic(err)
	}
}

func main() {
	go runRest()
	runGrpc()
}
